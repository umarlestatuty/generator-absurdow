import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  private wholeSentences = [
    'Spory pomiędzy uczniami a szkoła wynikłe z nieprzestrzegania niniejszego statutu rozstrzyga właściwy ze względu na nastrój dyrektora nauczyciel.',
    'Uczeń może otrzymać ocenę celującą z języka polskiego tylko, jeśli jest honorowym dawcą krwi.',
    'Uczeń może otrzymać ocenę celującą tylko, jeśli zna fakty z życia patronki szkoły.',
    'Uczniowie mający w szkole sympatię powinni pamiętać o stosownym zachowywaniu się.',
    'Uczniom zabrania się leżenia na korytarzu.',
  ];

  public sentencesToDisplay = [];

  ngOnInit() {
    this.generate();
  }

  public generate() {
    this.sentencesToDisplay = [];
    this.sentencesToDisplay.push(
      this.wholeSentences[this.getRandomInt(this.wholeSentences.length)]
    );
    this.addUczniowie();
    this.addZakaz();
    this.addObowiazek();
    this.addMusza();
    this.addMoga();
  }

  public getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  private universalPhase3 = [
    'w dni parzyste w miesiącu, kiedy urodziny ma pani dyrektor.',
    'chyba że sprzeciw wniósł dozorca szkolny.',
    'w dni, kiedy w stołówce podawany jest rosół.',
    'w czasie przerw na II piętrze.',
    'chyba że odbywa się koncert Zenka Martyniuka.',
    'chyba że pani bibliotekarka jest na L4.',
    'chyba że w szkole odbywa się deratyzacja.',
    'za wyjątkiem dni, kiedy są sprawdziany z {SUBJECT}.',
    'w dni, kiedy trwa zbiórka makulatury.',
    'ale dotyczy to tylko uczniów o imionach na litery: {LETTER}, L, I i H.',
    'w pracowni biologicznej w każdy wtorek po drugim czwartku miesiąca.',
    'ale nie dotyczy to osób urodzonych w maju i lipcu.',
    'w Tłusty Czwartek i pierwszy poniedziałek czerwca.',
    'w urodziny Maryli Rodowicz.',
  ];

  private getUniversalPhase3() {
    let universalEnd = this.universalPhase3;
    let letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'K', 'P', 'B', 'Z', 'J'];
    let subject = ['geografii', 'matematyki', 'fizyki'];

    universalEnd = universalEnd.map((sentence) => {
      sentence = sentence.replace(
        /\{LETTER\}/g,
        letters[this.getRandomInt(letters.length)]
      );
      sentence = sentence.replace(
        /\{SUBJECT\}/g,
        subject[this.getRandomInt(subject.length)]
      );
      return sentence;
    });

    return universalEnd;
  }

  private addUczniowie() {
    let start = 'Uczniowie';
    let middle = [
      'pełnoletni samodzielnie usprawiedliwiają nieobecności,',
      'pełnoletni samodzielnie wracają z wycieczki szkolnej,',
      'pełnoletni mogą zastrzec jawność ocen dla rodziców,',
      'mogą sami robić zakupy w sklepiku szkolnym,',
      'mogą poprawiać oceny z kartkówek,',
      'muszą dostarczyć wyciąg z konta bibliotecznego,',
      'mogą posiadać piórnik i nożyczki,',
      'nie mogą opuszczać terenu szkoły,',
    ];
    let end = [
      'jeśli w poprzednim miesiącu przynieśli minimum 25 kg nakrętek.',
      'po uprzedniej zgodzie księdza proboszcza.',
      'jeśli przyjeżdżają do szkoły na rowerze.',
      'jeśli są w stanie zrobić 50 pompek i lubią marchewkę z groszkiem.',
      'chyba że w roku ubiegłym złożyli co najmniej 2 skargi do kuratorium.',
      'jeśli mają minimum 170 cm wzrostu.',
    ];

    let universalEnd = this.getUniversalPhase3();
    end.push(...universalEnd);

    for (let i = 0; i < 1 + this.getRandomInt(2); i++) {
      let sentence = start + ' ';
      sentence = sentence + middle[this.getRandomInt(middle.length)] + ' ';
      sentence = sentence + end[this.getRandomInt(end.length)];
      this.sentencesToDisplay.push(sentence);
    }
  }

  private addZakaz() {
    let start = 'W szkole obowiązuje zakaz';
    let middle = [
      'używania telefonów,',
      'palenia papierosów,',
      'okazywania przynależności do subkultur,',
      'głoszenia swoich poglądów,',
      'zgłaszania spraw do kuratorium,',
      'poprawiania ocen,',
      'noszenia bluzek na ramiączkach,',
      'noszenia krótkich spodenek,',
      'farbowania włosów na nienaturalne kolory,',
      'zemianiania struktury włosa,',
      'czytania Mickiewicza, Tołstoja i Rimbauda,',
      'jedzenia ananasów, jabłek i trzymania w plecaku kasztanów,',
      'posiadania zegarków cyfrowych i malowania paznokci,',
      'malowania paznokci na zółto i na zielono,',
      'noszenia zbyt mocnego makijażu,',
      'wychodzenia do toalety,',
      'skakania przez okno,',
      'leżenia na korytarzu,',
    ];
    let end = [
      'jeśli zgody na to nie wyrazi minimum pięciu sołtysów.',
      'chyba że uczeń okaże paragon potwierdzający zakup minimum 5 owoców.',
      'w obrębie 5 metrów od drzwi do pokoju nauczycielskiego.',
      'ale dotyczy on tylko osób mających minimum 170cm wzrostu.',
    ];

    let universalEnd = this.getUniversalPhase3();
    end.push(...universalEnd);

    for (let i = 0; i < 1 + this.getRandomInt(2); i++) {
      let sentence = start + ' ';
      sentence = sentence + middle[this.getRandomInt(middle.length)] + ' ';
      sentence = sentence + end[this.getRandomInt(end.length)];
      this.sentencesToDisplay.push(sentence);
    }
  }

  private addObowiazek() {
    let start = 'W szkole istnieje obowiązek';
    let middle = [
      'zachowania ciszy i spokoju,',
      'posiadania kolorowego identyfikatora i smyczy,',
      'noszenia tylko szarych tornistrów,',
      'zachowania powagi należytej miejscu nauki,',
      'słuchania poleceń dyrektora,',
    ];
    let end = [
      'jeśli zgody na to nie wyrazi minimum pięciu sołtysów.',
      'chyba że uczeń okaże paragon potwierdzający zakup minimum 5 owoców.',
      'w obrębie 5 metrów od drzwi do pokoju nauczycielskiego.',
      'ale dotyczy on tylko osób mających minimum 170cm wzrostu.',
    ];

    let universalEnd = this.getUniversalPhase3();
    end.push(...universalEnd);

    for (let i = 0; i < 1 + this.getRandomInt(2); i++) {
      let sentence = start + ' ';
      sentence = sentence + middle[this.getRandomInt(middle.length)] + ' ';
      sentence = sentence + end[this.getRandomInt(end.length)];
      this.sentencesToDisplay.push(sentence);
    }
  }

  private addMusza() {
    let start = 'Uczniowie muszą';
    let middle = [
      'spożywać posiłki tylko na drugiej i piątej przerwie,',
      'czytać poezję Szymborskiej,',
      'sadzić wrzosy i tulipany wokół szkoły,',
      'pisać piórem z czarnym tuszem lub zieloną kredką,',
    ];
    let end = [];

    let universalEnd = this.getUniversalPhase3();
    end.push(...universalEnd);

    for (let i = 0; i < 1 + this.getRandomInt(1); i++) {
      let sentence = start + ' ';
      sentence = sentence + middle[this.getRandomInt(middle.length)] + ' ';
      sentence = sentence + end[this.getRandomInt(end.length)];
      this.sentencesToDisplay.push(sentence);
    }
  }

  private addMoga() {
    let start = 'Uczeń może';
    let middle = [
      'przynieść do szkoły toster,',
      'wnieść do szkoły chomika dwa razy w miesiącu,',
      'zgłosić nieprzygotowanie,',
      'wypożyczyć książkę z biblioteki,',
      'nie pisać sprawdzianu,',
      'chodzić w różowych włosach,',
      'śpiewać piosenki disco polo,',
    ];
    let end = [];

    let universalEnd = this.getUniversalPhase3();
    end.push(...universalEnd);

    for (let i = 0; i < 1 + this.getRandomInt(1); i++) {
      let sentence = start + ' ';
      sentence = sentence + middle[this.getRandomInt(middle.length)] + ' ';
      sentence = sentence + end[this.getRandomInt(end.length)];
      this.sentencesToDisplay.push(sentence);
    }
  }
}
